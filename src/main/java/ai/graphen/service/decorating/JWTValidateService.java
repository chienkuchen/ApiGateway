package ai.graphen.service.decorating;

import ai.graphen.util.Config;
import com.linecorp.armeria.common.HttpHeaderNames;
import com.linecorp.armeria.common.HttpRequest;
import com.linecorp.armeria.common.HttpResponse;
import com.linecorp.armeria.common.HttpStatus;
import com.linecorp.armeria.server.Service;
import com.linecorp.armeria.server.ServiceRequestContext;
import com.linecorp.armeria.server.SimpleDecoratingService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.impl.Base64Codec;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JWTValidateService extends SimpleDecoratingService<HttpRequest, HttpResponse> {
  private static Logger logger = LoggerFactory.getLogger(JWTValidateService.class);

  public JWTValidateService(Service<HttpRequest, HttpResponse> delegate) {
    super(delegate);
  }

  @Override
  public HttpResponse serve(ServiceRequestContext ctx, HttpRequest req) throws Exception {
    try {
      String token = req.headers().get(HttpHeaderNames.AUTHORIZATION).split(" ")[1];

      Jwts.parser().setSigningKey(getPublicKey()).parseClaimsJws(token);

      Service<HttpRequest, HttpResponse> delegate = delegate();
      return delegate.serve(ctx, req);
    } catch (SignatureException e) {
      logger.error(String.format("SignatureException: %s", e.getMessage()));
      return HttpResponse.of(HttpStatus.UNAUTHORIZED);
    } catch (Exception e) {
      logger.error(String.format("Unexpected exception: %s", e.getMessage()));
      throw e;
    }
  }

  private PublicKey getPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
    byte[] encodedPublicKey = new Base64Codec().decode(Config.getJWTPublicKey());
    X509EncodedKeySpec spec = new X509EncodedKeySpec(encodedPublicKey);
    return KeyFactory.getInstance("RSA").generatePublic(spec);
  }
}
