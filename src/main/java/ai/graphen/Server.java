package ai.graphen;

import ai.graphen.service.PingPong;
import ai.graphen.service.decorating.JWTValidateService;
import ai.graphen.util.Config;
import com.linecorp.armeria.common.SessionProtocol;
import com.linecorp.armeria.server.ServerBuilder;
import com.linecorp.armeria.server.logging.LoggingService;

public class Server {
  public static void main(String[] args) {

    ServerBuilder sb;
    sb = new ServerBuilder();
    sb.port(Config.getServerPort(), SessionProtocol.HTTP);
    sb.annotatedService(new PingPong())
        .decorator(JWTValidateService::new)
        .decorator(LoggingService.newDecorator());
    sb.build().start().join();
  }
}
