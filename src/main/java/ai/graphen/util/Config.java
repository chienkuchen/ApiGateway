package ai.graphen.util;

import com.typesafe.config.ConfigFactory;

public class Config {
  private static com.typesafe.config.Config conf = ConfigFactory.load();

  public static int getServerPort() {
    return conf.getInt("ServerPort");
  }

  public static String getJWTPublicKey() {
    return conf.getString("JWTPublicKey");
  }
}
