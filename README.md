# System requirements
* Java 1.8


# Technical stack:
* Asynchronous RPC/REST library built on top of Java 8, Netty, HTTP/2, Thrift and gRPC: [armeria](https://line.github.io/armeria/)
* Java JWT: JSON Web Token for Java and Android [jjwt](https://github.com/jwtk/jjwt)

# How do build?
```bash
./gradlew build
```

# How do test?
```bash
./gradlew test
```

# How do build a docker image?
```bash
./gradlew dockerBuildImage
```

# How do run?
```bash
./gradlew run
```

# How do run from a docker image?
```bash
docker run -i -p 8080:8080 graphen/api-gateway:latest
```

# How do access a API?
*Start* a server at first
```bash
curl -X GET -H "Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.TCYt5XsITJX1CxPCT8yAV-TVkIEq_PbChOMqsLfRoPsnsgw5WEuts01mq-pQy7UJiN5mgRxD-WUcX16dUEMGlv50aqzpqh4Qktb3rk-BuQy72IFLOqV0G_zS245-kronKb78cPN25DGlcTwLtjPAYuNzVBAh4vGHSrQyHUdBBPM" -H "Cache-Control: no-cache" "http://localhost:8080/ping"
// return pong
```

```bash
curl -X GET -H "Authorization: Bearer malicioustoken" -H "Cache-Control: no-cache" "http://localhost:8080/ping"
// return 401 Unauthorized
```  